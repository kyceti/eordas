
<style>
table {
    width: 100%;
    align: center;
    margin-left:1.00rem;
    font-size:0.75rem;
}

th:hover,td:hover{
    background-color: #3d3d3d;
}


h1,h2,h3,h4,h5,h6,p,ul,ol,tr {
    font-family:"Cambria"
}
h1 {
    font-weight:bold;
    font-size:2.00rem;
    text-align:center;
}
h2 {
    font-weight:bold;
    font-size:1.50rem;
    margin-left:1.00rem;
    text-align:center;
    border-bottom: 2px solid gray;
}
h3 {
    font-weight:bold;
    font-size:1.20rem;
    margin-left:1.00rem;
    border-bottom: 1px solid gray;
}
h4 {
    font-weight:bold;
    font-size:1.10rem;
    margin-left:1.00rem;
}
h5 {
    font-weight:bold;
    font-size:1.00rem;
    margin-left:1.00rem;
}
h6 {
    font-weight:normal;
    font-size:1.00rem;
    margin-left:1.00rem;
}
p {
    font-size:0.75rem;
    margin-left:1.00rem;
    text-align:justify;
}

ul {
    list-style-type: circle;
    margin-bottom:5px;
}

ol {
    list-style-type: numeric;
    margin-bottom:5px;
}

</style>

* [Back to Title](../README.md)

# Appendix A - List of Creatures
???

## A.1 - Playable creatures
Ancestral feats are feats that you can take to augment and enhance

### Hume
*Scientific name*

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur orci nulla, in finibus orci sodales convallis. Proin ac felis eget enim rhoncus tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean vel libero id lectus lobortis ultricies in vitae ligula. Maecenas diam lectus, faucibus non dapibus et, vestibulum at quam. Cras in orci efficitur, placerat libero quis, pellentesque lectus. Mauris cursus magna in tempor finibus.

<div style="column-count:3">

#### Powers 
| Power | Base Rank | Max. Rank |
| :------- | :--------: | :--------: |
| Agility | 10 (7+1d6) | 99         |
| Attunement | 10 (7+1d6) | 99         |
| Insight | 10 (7+1d6) | 99         |
| Strength | 10 (7+1d6) | 99         |

When a hume takes a feat, they gain 2 (1d4) ranks to powers of their choice.
***Agility.*** 10 (7+1d6) base; max. 99
***Attunement.*** 10 (7+1d6) base; max. 99
***Insight.*** 10 (7+1d6) base; max. 99
***Strength.*** 10 (7+1d6) base; max. 99

#### Vitalities
| Vitality | Base Value | Max. Value |
| :------- | :--------- | :--------- |
| Vigour   | 5 (3+1d4)  | 70         |
| Clarity  | 5 (3+1d4)  | 70         |
| Posture  | 5 (3+1d4)  | 70         |

#### Attributes
When a hume takes a feat, they gain 3 (1d6) points to vitalities of their choice.
***Vigour.*** 5 (3+1d4) base; max. 70
***Clarity.*** 5 (3+1d4) base; max. 70
***Posture.*** 5 (3+1d4) base; max. 70

</div>








