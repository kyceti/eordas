
<style>
h1,h2,h3,h4,h5,h6,p,ul,ol {
    font-family:"Cambria"
}
h1 {
    font-weight:bold;
    font-size:2.00rem;
    text-align:center;
}
h2 {
    font-weight:bold;
    font-size:1.50rem;
    margin-left:1.00rem;
    text-align:center;
    border-bottom: 2px solid gray;
}
h3 {
    font-weight:bold;
    font-size:1.20rem;
    margin-left:1.00rem;
    border-bottom: 1px solid gray;
}
h4 {
    font-weight:bold;
    font-size:1.10rem;
    margin-left:1.00rem;
}
h5 {
    font-weight:bold;
    font-size:1.00rem;
    margin-left:1.00rem;
}
h6 {
    font-weight:normal;
    font-size:1.00rem;
    margin-left:1.00rem;
}
p {
    font-size:1.00rem;
    margin-left:1.00rem;
    text-align:justify;
}

ul {
    list-style-type: circle;
    margin-bottom:5px;
}

ol {
    list-style-type: numeric;
    margin-bottom:5px;
}

</style>

* [Back to Title](../README.md)

## Appendix C - List of Feats
Feats are the way that you tangibly can grow your character.

### C.1 - Ancestral Feats
Ancestral feats are feats that you can take to augment and enhance



#### Move
**Speed:** -4 penalty
##### Declaring this Action
You can take the Move action as many times as you wish per round.



