# Powers
Characters have two primary forms of determining the effectiveness of their actions; Powers are one of these forms. In comparison to the other form, *Stats*, powers are **immutable** during a session, and can only be changed through character development or growth, such as taking feats or undergoing training. As such, they act as a 'baseline' for your character's ability.

### Properties of Powers
Powers are represented by a number from 0 to 100, known as a **Rank**. There is also an associated number known as the **Power Bonus**, given by the power in particular.

You can use your power in two ways:
* ***Power Check.*** Roll a d20. Add the Power Bonus to your roll. If the roll equals or exceeds a given Difficulty Check (DC), you succeed in the check.
* ***Placeholder.*** WORK IN PROGRESS

### Types of Powers

There are a few main categories of powers:
* ***Base Powers.*** Innate powers available to every creature.
* ***Skill Powers.*** Powers that have to be learned or studied in order to be used. Generally are enhancements of Base Powers that are more specific, with better power bonuses.
* ***Ideological Powers.*** Powers that measure your allegiance, beliefs, honour, or opinion on various things. These powers generally can be used regardless of their rank once invested into, as their power bonus sways from positive to negative.

## Base Powers
Base powers are powers that every creature and being capable of autonomy, of some degree. They are non-specific but highly versatile, and are used when you don't have a more specific power you can use.

Due to their general usefulness, base powers are harder to increase - they can only be developed through Growth, rather than Feats.  Additionally, their **Power Bonus** is equal to one-fifth of the Rank (rounded down), to a maximum of +20 at Rank 100.

There are four base powers: Strength, Agility, Insight, and Attunement.

#### Strength (STR)
Strength is your physical prowess; your ability to exert force and pressure on your surroundings. It is also your strength of *character*, making you more menacing and intimidating.

Strength increases your attack power with weaponry that uses your physical being, your maximum *Posture*, the amount of gear you can carry, and your ability to push, grapple, and perform athletic actions.

#### Base Power: Agility (AGI)
Agility is your overall acuity of body; your quickness of muscle and mind. 

Agility is used to determine turn order within a round, allowing you to act before foes can react; it also improves your verbal wits, your instincts, and ability to think on your toes.

#### Base Power: Insight (INS)
Insight is your overall cognition and awareness of your environment. Insight is a major factor in determining a creature's intelligence; creatures below 4 Insight are not sapient, or aware of their surroundings, and creatures below 8 Insight lack the sentience to act outside their instincts.

Insight increases your maximum *Clarity*, your magickal potential, your academic intelligence and knowledge, and your *Truesight* on the world.

#### Base Power: Attunement (ATN)
Attunement is your mastery of your spirit and body. It governs how well you can use what you have at your disposal; unlike your wits and awareness, it is your *awakened potential*. Monastarians, scions, and sorcerers are folk who have awakened their attunement.

Attunement increases your maximum *Vigour*, your balance and ability to concentrate, your effectiveness with techniques, and innate potential.

## Skill Powers
Skill powers are powers that are learned through training or study. They generally require you to be sapient (4 Insight), although you need not be sentient (8 Insight). Skills are more easily increased than base powers but are more specific, limiting what you can do with them to specific actions or tasks, which are described within each skill.

You can use a skill instead of a base power when a Power Check is asked for an action that is relevant to the skill. The DM will determine whether your requested skill is appropriate or not.

You can't use a skill unless you have at least a single rank in that skill.




