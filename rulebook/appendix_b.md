
<style>
h1,h2,h3,h4,h5,h6,p,ul,ol {
    font-family:"Cambria"
}
h1 {
    font-weight:bold;
    font-size:2.00rem;
    text-align:center;
}
h2 {
    font-weight:bold;
    font-size:1.50rem;
    margin-left:1.00rem;
    text-align:center;
    border-bottom: 2px solid gray;
}
h3 {
    font-weight:bold;
    font-size:1.20rem;
    margin-left:1.00rem;
    border-bottom: 1px solid gray;
}
h4 {
    font-weight:bold;
    font-size:1.10rem;
    margin-left:1.00rem;
}
h5 {
    font-weight:bold;
    font-size:1.00rem;
    margin-left:1.00rem;
}
h6 {
    font-weight:normal;
    font-size:1.00rem;
    margin-left:1.00rem;
}
p {
    font-size:1.00rem;
    margin-left:1.00rem;
    text-align:justify;
}

ul {
    list-style-type: circle;
    margin-bottom:5px;
}

ol {
    list-style-type: numeric;
    margin-bottom:5px;
}

</style>

* [Back to Title](../README.md)

## Appendix B - List of Actions
Actions are the way you can interact with the world. They are a tangible act that can be enhanced and augmented through character growth. The number of actions you perform is ultimately up to you, however each action takes up part of your *Speed* and having too low of a Speed roll causes you to move last or possibly even fail.

### B.1 - Basic Actions
Basic actions are actions that most beings have access to.



#### Move
**Speed:** -4 penalty
##### Declaring this Action
You can take the Move action as many times as you wish per round.
##### Realising this Action
You move a distance in space equal to your Pace. The medium through which you are moving requires a Pace suited for that medium.
If you move out of range of a creature that is targeting you before they act, you dodge the attack.




#### Talk
*Cost:* -4 to Swiftness check
*Requirements:* Has not taken Talk this round
You make various sounds with your body in an attempt to communicate. You can speak a language you know, mimic one, or use non-specific gestures to convey meaning. This communication is generally no more than a sentence in length, and depending on the content, you can perform one of the following acts:
* **Cooperate.** You can suggest to an allied character that can hear you to do a specific action. If that character performs that action next round, they gain a +10 to any checks they make for that action.


