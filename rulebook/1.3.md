
<style>
h1,h2,h3,h4,h5,h6,p,ul,ol {
    font-family:"Cambria"
}
h1 {
    font-weight:bold;
    font-size:2.00rem;
    text-align:center;
}
h2 {
    font-weight:bold;
    font-size:1.50rem;
    margin-left:1.00rem;
    text-align:center;
    border-bottom: 2px solid gray;
}
h3 {
    font-weight:bold;
    font-size:1.20rem;
    margin-left:1.00rem;
    border-bottom: 1px solid gray;
}
h4 {
    font-weight:bold;
    font-size:1.10rem;
    margin-left:1.00rem;
}
h5 {
    font-weight:bold;
    font-size:1.00rem;
    margin-left:1.00rem;
}
h6 {
    font-weight:normal;
    font-size:1.00rem;
    margin-left:1.00rem;
}
p {
    font-size:1.00rem;
    margin-left:1.00rem;
    text-align:justify;
}

ul {
    list-style-type: circle;
    margin-bottom:5px;
}

ol {
    list-style-type: numeric;
    margin-bottom:5px;
}

</style>

* [Back to Title](../README.md)
## 1.3 - Actions
In either mode of play, you interact with your environment using Actions. This goes down from as general and moving, talking, and grabbing objects, to more specific tasks such as legerdemain, attacking, and performing techniques.

Actions might seem restrictive, but they provide a framework for you to justify your acts in codified reality. This puts more power in the hands of the players than the GM.

### 1.3.1 - Action Cards
As the number of actions you can perform grows as you develop your skills and techniques, you can choose to print out actions as poker-size cards that concisely detail what you can do in an action, how to determine success, and the effects of an action.

### 1.3.2 - How to Use Actions

In encounters, you declare actions after the GM has described the conflict, and any actions that NPC's are declaring. Declaration of an action means that you commit to performing the action this round, even if it fails. 

You can declare one or more actions per round. After declaring actions, actions are realised as follows:
1. **All characters make a Speed check.** The Speed check is an Agility check, plus all bonuses from actions, and minus any penalties from actions. A critical failure on Speed means you *fumble* and do nothing that round.
2. **Characters act in order of their Speed.** Each character performs all of their actions when it is their turn in the round. A character that has declared more than one action can perform them in any order they wish, so long as the actions can be performed in that order.

### 1.3.3 - Basic Actions
There are some actions that most beings can perform. These actions do not require any prior knowledge, only the means in order to perform them. For example, in order to move, you will need a movement speed.

For a full list of actions, including basic actions, see [Appendix B](appendix_b.md).
