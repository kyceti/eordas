
<style>
h1,h2,h3,h4,h5,h6,p,ul,ol {
    font-family:"Cambria"
}
h1 {
    font-weight:bold;
    font-size:2.00rem;
    text-align:center;
}
h2 {
    font-weight:bold;
    font-size:1.50rem;
    margin-left:1.00rem;
    text-align:center;
    border-bottom: 2px solid gray;
}
h3 {
    font-weight:bold;
    font-size:1.20rem;
    margin-left:1.00rem;
    border-bottom: 1px solid gray;
}
h4 {
    font-weight:bold;
    font-size:1.10rem;
    margin-left:1.00rem;
}
h5 {
    font-weight:bold;
    font-size:1.00rem;
    margin-left:1.00rem;
}
h6 {
    font-weight:normal;
    font-size:1.00rem;
    margin-left:1.00rem;
}
p {
    font-size:1.00rem;
    margin-left:1.00rem;
    text-align:justify;
}

ul {
    list-style-type: circle;
    margin-bottom:5px;
}

ol {
    list-style-type: numeric;
    margin-bottom:5px;
}

</style>

* [Back to Title](../README.md)

## Appendix A - List of Powers
Powers, or *passive abilities*, are immutable values assigned to your character that you can use to determine your success with different actions you choose to make.

### A.1 - Base Powers
Base powers are the innate powers present across every being. They have a lower check bonus than other powers on a per-rank basis, but they are also more applicable to many situations, and can be used in place of a skill for most actions.

There are four base powers - Agility, Attunement, Insight, and Strength.

#### Agility
Agility is your quickness of both muscle and mind. It governs your reflexes and instinct to situations, as well as your quickness of tongue, such as in social situations. 

##### Agility Check
You can perform an Agility check

##### Related Actions