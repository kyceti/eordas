# Tome of Eordas
The Tome of Eordas project plays three roles. It is:

* A **rulebook**, describing the rule-set used to play this game;
* An **encyclopaedia**, detailing the creatures, tools, and lands of Eordas; and
* A **chronicle**, detailing the events of Eordas, including session histories, and the impacts on history caused by the game.

## Repository Structure
The tome is separated into these three primary parts, kept independent of each other. For most intents and purposes, you will be consulting the *rulebook* the most, with the *encyclopaedia* and the *chronicle* acting as reference material should you need to know more information on a specific beast, feat, ability, or recall events.

### Part 1: Rulebook
The rulebook is structured to cater to new players and characters first, and then become more complex to detail more esoteric features; Powers, vitals, and other features of the game are detailed simplistically and most saliently, followed by other features which are generally hidden behind certain choices, such as what ancestry or adventuring style you choose.

* [1.1 - How to Play](rulebook\1.1.md)
* [1.2 - Powers, Vitals, and Checks](rulebook\1.2.md)
* [1.3 - Actions](rulebook\1.3.md)
* [Appendix A: List of Powers](appendix_a.md)
* [Appendix B: List of Actions](appendix_b.md)

### Part 2: Encyclopaedia
The encyclopaedia is separated into different chapters 0that categorise 'things' into similar groupings. These include:
* Sapient beings, such as *peoples*, *animals*, *plants*, and *monsters*.
* Natural objects, such as *rocks*, *metals*, *chitin*, and *air*.
* Artificial objects, such as *weaponry*, *clothing*, and *magickal artefacts*.
* Geographies, such as *plains*, *mountains*, and *valleys*.
* Boundaries, such as *kingdoms*, *villages*, and *shires*.

### Part 3: Chronicle
The chronicle keeps a yearly record on happenings in the world. Note that all information is stored here, but this tome is not easily visible to all characters, so you will have to make requests to the DM if you wish for your character to know information from the chronicle; and such a request would need to be reasonably sourced. This could be done through research or lived experience, for example.

However, as a player, you can appreciate and read the chronicle to your own desires.