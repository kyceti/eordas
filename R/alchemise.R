

alchemise <- function(fibre,substanceID1,substanceID2,docs=FALSE) {
  
  # The alchemise function will produce an alchemy of two substances from the current FIBRE table.
  # Given the fibre table, IF the substance is physically possible, the function binds a row to the table.
  
  # Get alchemies
  # Swap order based on which has more cordus - it is the reagant
  heavier_fibre <- ifelse.(filter.(fibre,SubstanceID==substanceID1)$Cordus > filter.(fibre,SubstanceID==substanceID2)$Cordus,substanceID1,substanceID2)
  lighter_fibre <- ifelse.(heavier_fibre == substanceID1,substanceID2,substanceID1)
  print(paste(heavier_fibre,lighter_fibre))
  
  
  reagant <- fibre %>% filter.(SubstanceID == heavier_fibre)
  reactant <- fibre %>% filter.(SubstanceID == lighter_fibre)
  
  ###########################
  # Composition
  ###########################
  
  # Use the GCD function to determine the ratio of the reagant and reactant.
  num_reagant <- max(replace_na(reactant$Cordus/pracma::gcd(reagant$Cordus,reactant$Cordus),1),1)
  num_reactant <- max(replace_na(reagant$Cordus/pracma::gcd(reagant$Cordus,reactant$Cordus),1),1)
  
  ## Alchemical Formulae/Name
  if (reagant$SubstanceID == reactant$SubstanceID) {
    # If the two substances are identical, then produce the Stable Lattice; this depends on what the arrangement of Elements of Heart
    alchemical_name <- paste0("di",tolower(reagant$`Alchemical name`),"ium")
    alchemical_formula <- paste0(reagant$Symbol,"2")
    
    # TODO: PLACEHOLDER - use 2 as lattice
    num_reagant <- 1
    num_reactant <- 1
    
    # Lattice Types:
    # 
    # Alcor (Solid) - 7
    # Nicor (Liquid) - 5
    # Yacor (Gas) - 3
    # Wacor (Plasma) - 2
    # Anima (Mist) - 1
    lattice <- "Lattice-based"
  } else {
    # If the two substances are different, use the ratios to produce a correct representation.
    prefix <- c("","di","tri","tetra","penta","hexa")
    suffix <- c("ide")
    alchemical_name <- paste0(prefix[num_reagant],tolower(reagant$`Alchemical name`)," ",prefix[num_reactant],tolower(reactant$`Alchemical name`),suffix)
    alchemical_formula <- paste0(reagant$Symbol,ifelse(num_reagant!=1,num_reagant,""),reactant$Symbol,ifelse(num_reactant!=1,num_reactant,""))
    lattice <- "Covalescence"
  }
  
  # Meme
  
  # Calculate total Cordus/Animus
  
  total_cordus <- reagant$Cordus*num_reagant + reactant$Cordus*num_reactant
  total_animus <- reagant$Animus*num_reagant + reactant$Animus*num_reactant
  cordus_stable_max <- floor(total_cordus^1.02)
  
  
  #######################
  # Fibre Categories
  #######################
  # Category is either Anima, or Element depending on total_cordus and total_animus
  category <- ifelse.(
    total_cordus == 0, "Anima",
    ifelse.(
      total_animus == 0, "Corra",
      ifelse.(
        reagant$Category == "Element" & reactant$Category == "Element" & reagant$SubstanceID != reactant$SubstanceID, "Compound",
        "Element"
      )
    )
  )
  stability <- ifelse.(total_animus < total_cordus,"Siphotopic",ifelse.(total_animus <= cordus_stable_max,"Stable","Isotopic"))
  alchemy <- ifelse.(reagant$Category == "Corra",1,0)
  
  # Documentation
  if (docs) {
    print(glue::glue(
      "
    Alchemical analysis:
    {num_reagant}x {reagant$`Alchemical name`} | {reagant$Stability} {reagant$Category} ({reagant$Symbol}) |  {reagant$Cordus}C / {reagant$Animus}A | {reagant$Potentia} Potential
    {num_reactant}x {reactant$`Alchemical name`} | {reactant$Stability} {reactant$Category} ({reactant$Symbol}) | {reactant$Cordus}C / {reactant$Animus}A | {reactant$Potentia} Potential
    ---
    Alchemy result:
    {alchemical_name} - {stability} {category} ({alchemical_formula}) | {num_reagant*reagant$Cordus + num_reactant*reactant$Cordus}C / {num_reagant*reagant$Animus + num_reactant*reactant$Animus}A | {num_reagant*reagant$Potentia + num_reactant*reactant$Potentia} Potential
    "
    ))
  }
  
  #######################
  # Data Extrapolation
  #######################
  tidytable(
    SubstanceID = NA,
    `Alchemical name` = alchemical_name,
    `Common name` = NA,
    Symbol = alchemical_formula,
    Category = category,
    Stability = stability,
    Alchemy = alchemy,
    Lattice = lattice,
    State = NA,
    Skein = num_reagant*(reagant$Cordus + reagant$Animus) + num_reactant*(reactant$Cordus + reactant$Animus),
    Cordus = num_reagant*reagant$Cordus + num_reactant*reactant$Cordus,
    Animus = num_reagant*reagant$Animus + num_reactant*reactant$Animus,
    Nicor = num_reagant*reagant$Nicor + num_reactant*reactant$Nicor,
    Alcor = num_reagant*reagant$Alcor + num_reactant*reactant$Alcor,
    Yacor = num_reagant*reagant$Yacor + num_reactant*reactant$Yacor,
    Wacor = num_reagant*reagant$Wacor + num_reactant*reactant$Wacor,
    Ilimpa = num_reagant*reagant$Ilimpa + num_reactant*reactant$Ilimpa,
    Qullpa = num_reagant*reagant$Qullpa + num_reactant*reactant$Qullpa,
    Azupa = num_reagant*reagant$Azupa + num_reactant*reactant$Azupa,
    Imapa = num_reagant*reagant$Imapa + num_reactant*reactant$Imapa,
    Potentia = num_reagant*(reagant$Cordus - reagant$Animus) + num_reactant*(reactant$Cordus - reactant$Animus)
  )
}