strand <- function(fibre_row,xrange=c(-10,10)) {
  # Build a model for the strand
  output_data <- seq(from=xrange[1],to=xrange[2],by=0.01) %>% as_tidytable()
  
  # Create y based on the fibre
  output_data %>% 
    mutate.(
      y = (fibre_row$Nicor*cos(x) + fibre_row$Alcor*acos(x/10) + fibre_row$Yacor*sin(x/10) + fibre_row$Wacor*asin(x/10))*(atan(x)*fibre_row$Ilimpa+atan(-1*x)*fibre_row$Azupa+x*atan2(fibre_row$Qullpa,fibre_row$Imapa))
    ) %>% ggplot(aes(x=x,y=y)) + geom_line()
}